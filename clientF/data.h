#define USERNAME_LEN 20

#define PASSWORD_LEN 20

#define DIRECTORY_LEN 120

#define BUFFER_LEN 600

#define MESSAGE_LEN (BUFFER_LEN - DIRECTORY_LEN - USERNAME_LEN - sizeof(int))

#define MIN(a,b) ((a) < (b) ? a : b)


struct msg {
      int length;
      char directory[DIRECTORY_LEN];
      char username[USERNAME_LEN];     
      char message[1];
};
                                               