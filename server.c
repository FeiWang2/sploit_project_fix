
#include "sploit.h"

void dostuff(int, char*, char*); /* function prototype */
void up_dir(char* initial_dir); 
void parseCD(char* local_dir, char* token, char* root, int);
int writeSockNoEnd(int sock, char* message);
int writeSock(int sock, char* message);
int getUserPort(char* username);
void commandNotComplete(int sock);
int login(char* loghist, char* username);
int logout (char* loghist, char* username);
bool Islogin(char* loghist, char* username) ;
char* whoIsIn(char* loghist);

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     // these are for TCP connection
     int sockfd, newsockfd, pid;
     int portno;
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;

     // read port no and root directory, log-histroy directory from config file
     char root[ROOT_LEN]; bzero(root, ROOT_LEN);
     char loghist[ROOT_LEN]; bzero(loghist, ROOT_LEN);
     char string_0[LINE_LEN];
     char header[HEADER_LEN];
     char message1[MESSAGE1_LEN];

     FILE * file = fopen("sploit.conf","r+");

     while((fgets(string_0,LINE_LEN-1,file)) != NULL) {

      sscanf(string_0,"%s", header);
      if(strncmp(header, "base", HEADER_LEN-1) == 0) {
        sscanf(string_0, "%s %s", header, message1);
        strncpy(root, message1, ROOT_LEN-1); 
        continue;
      } 
      if (strncmp(header, "loghist", HEADER_LEN-1) == 0) {
        sscanf(string_0, "%s %s", header, message1);
        strncpy(loghist, message1, ROOT_LEN-1);
      }
      if(strncmp(header,"port", HEADER_LEN-1) == 0)  {
        sscanf(string_0, "%s %s", header, message1);
        portno = atoi(message1); break;
      } 
     }

     fclose(file);
     if (portno < 0) error("ERROR: failed to get port number from config file");

     // if root directory and log history directory is not here, create them      
     struct stat dir = {0};
     if (stat(root, &dir) == -1) 
       if (mkdir(root, 0700) < 0) error("ERROR: error in make directory\n");
     if (stat(loghist, &dir) == -1)
       if (mkdir(loghist, 0700) < 0) error ("ERROR: error in make directory\n");
    
     // continue with TCP connection
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     // binding to the port doesn't stuck if i do this
     int iSetOption = 1;
     setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&iSetOption, sizeof(iSetOption));
     
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     
     // while loop, fork happens in here
     while (1) {

              newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
              if (newsockfd < 0) error("ERROR on accept");
              //printf ("%s\n", "got an initial contact");

              // now dealing with accept function (new TCP connection in new process)             
              pid = fork();
              if (pid < 0) error("ERROR on fork");
              if (pid == 0)  {
                  // this is the child process
                  close(sockfd); // close the sockfd, since this child process is going to listen on the newsockfd
                  dostuff(newsockfd, root, loghist);
                  dprint("process returned!\n");
                  fflush(stdout);
                  exit(0);
              }
              // this is the main process
              close(newsockfd);
         } /* end of while */

     // if server quit
     close(sockfd);
     return 0; /* we never get here */
}

/******** DOSTUFF() *********************
 There is a separate instance of this function 
 for each connection.  It handles all communication
 once a connnection has been established.
 *****************************************/
struct StateOfClient {
  int status; //0: undefined user, 1: username given, but no password yet: 2: autheticated
  char username[USERNAME_LEN];
  char directory[DIRECTORY_LEN];  
  int port; // port of this user for getting file, port + 1 will be used for sending file
};



void dostuff (int sock, char* root, char* loghist)
{
   int n;
   char buffer[BUFFER_LEN];
   char delim[] = " ;\n\t\r";


   
   // local structure that tracks the authetication status
   struct StateOfClient states;
   states.status = 0;
   states.port = 0;
   bzero(states.username, USERNAME_LEN);
   bzero(states.directory, DIRECTORY_LEN);   
   
   

   while (1) {
    
    bzero(buffer,BUFFER_LEN);
    dprint("server back to beginning! with reset of buffer\n");

    // this function read from the sock until a newline, with infinite waiting time
    int aa = readUntilNewLineTime(sock, buffer, BUFFER_LEN, 0);
    if (aa == -1) // read too much without newline char
    {
      writeSock(sock, "Command too long! Make sure that command ends with newline character!\n");
      continue; // reset this process 
    }
    if (aa == -2) // read error
    {
      perror("ERROR reading from socket, connection probably lost!\n");
      if (states.status == 2) logout(loghist, states.username);
      break; // end this process
    }    

    dprint("COMMAND_CHECK: %s\n", buffer);

    // tokenize the command
    char* token = strtok(buffer, delim);
    if (!token) {
      writeSock(sock, "ERROR : please input your command and end with newline\n");
      continue;
    }
    
    // now deal with different command differently
    if (strncmp(token, "exit", strlen("exit")) == 0) {
          
          if (states.status == 2) {
            if (logout(loghist, states.username) < 0) {
              writeSock(sock, "ERROR Cannot remove the user from logged in list.\n \
                Contact the admin if you cannot re-login :)\n");
              close(sock); return;
            }
          }

      writeSock(sock, "goodbye!\n");
      close(sock); 
      dprint("return to upper level function\n"); fflush(stdout);
      exit(0);

    } 

    else if (strncmp(token, "ping", strlen("ping")) == 0) {

      token = strtok(NULL, delim);
      if (!token) {
        commandNotComplete(sock); continue;
      }

      // just use system ping function!!
      char command[BUFFER_LEN] = {0};
      // watch out if the hostname is too long
      if (strlen(token) > BUFFER_LEN - 1 - strlen("ping  -c 1")) {
        writeSock(sock, "ERROR : Host name too long.\n"); continue;
      }
      snprintf(command, BUFFER_LEN-1, "ping %s -c 1", token);
      dprint("the ping command is %s\n", command); 
      
      /* Open the command for reading. */
      FILE *fp = popen(command, "r");
      if (fp == NULL) {
        perror("ERROR cannot popen\n");
        writeSock(sock, "ERROR : cannot do popen() for ping command\n");
        continue;
      }

      /* Read the output a line at a time - output it. */
      /* need timeout machanism here ! */
      int filedesc = fileno(fp);
      fd_set set;
      struct timeval timeout;     

      FD_ZERO(&set); /* clear the set */
      FD_SET(filedesc, &set); /* add our file descriptor to the set */

      timeout.tv_sec = 1;
      timeout.tv_usec = 0;
      do {
            int rv = select(filedesc + 1, &set, NULL, NULL, &timeout);
            if(rv == -1)
            { perror("select"); close(filedesc); pclose(fp); close(sock); exit(1); } /* an error accured */
            else if(rv == 0)
            { 
              dprint("timeout from system ping call\n"); 
              writeSock(sock, "Server Time Out! reply maybe not complete"); // this adds a null char to sock
              close(filedesc); break; 
            }   /* a timeout occured, nothing to print */
            bzero(buffer, BUFFER_LEN);
            int nbyte = read(filedesc, buffer, BUFFER_LEN-1);
            if (buffer[nbyte-1] == '\0') {
                dprint("syscall terminated with a null char\n");
                writeSockNoEnd(sock, buffer);
                writeSock(sock, ""); // this adds a null char to sock
                close(filedesc); break;   
            }
            writeSockNoEnd(sock, buffer);
        
      } while(1);


      /* close */
      pclose(fp);
      continue;

    } 

    else if (strncmp(token, "login", strlen("login")) == 0) {
      
        if (states.status == 2) {
            // must reject if already log in!!
            char* message = "Already log in! If you want to change user, first use command: log out";
            writeSock(sock, message);  continue;
        }

        token = strtok(NULL, delim);  
        if (!token) {commandNotComplete(sock); continue;}

        strncpy(states.username, token, USERNAME_LEN-1);  
        states.status = 1;
        char* message = "\nNow provide the password by command: pass <password>";
        writeSockNoEnd(sock, "Hello "); writeSockNoEnd(sock, states.username);
        writeSock(sock, message);
        continue;

    } 

    else if (states.status < 1) {
      // ask for username before allowing anything below
      char* message = "Need log in first. Please provide your user name by command: login <username>";
      writeSock(sock, message);
      continue;

    } 

    else if (strncmp (token, "pass", strlen("pass")) == 0) {

      if (states.status == 2) {
        // already log in , reject command
        char* message = "Already log in! command rejected!";
        writeSock(sock, message);
        continue;
      } 

        token = strtok(NULL, delim); 
        if (!token) {commandNotComplete(sock); continue;}

        // now hash the password and compare 
        char digest_var[DIGEST_VAR_LEN];
        bzero(digest_var, DIGEST_VAR_LEN);
        if (hash_sha256(token, digest_var) < 0) {
          writeSock(sock, "ERROR something is wrong with hash. Command aborted.");
          continue;
        }

        char stored_digest[DIGEST_VAR_LEN];
        bzero(stored_digest, DIGEST_VAR_LEN);
        if (check_user(states.username, stored_digest) < 0)  {
          writeSock(sock, "ERROR something is wrong with retreaving password_hash. Command aborted.");
          continue;
        }
        
        if (strncmp(digest_var, stored_digest, DIGEST_VAR_LEN-1) == 0) {
          // check if this username-password pair is already in session
          if (Islogin(loghist, states.username)) {
            char* message = "This username_password is already in session. No repeated login allowed!";
            states.status = 0;
            bzero(states.username, USERNAME_LEN); 
            writeSock(sock, message); continue;
          }

          // Mark this user as logged in
          if (login(loghist, states.username) < 0) {
            writeSock(sock, "ERROR failed to mark the user as logged in. Login Aborted.");
            continue;
          }

          states.status = 2;
          bzero(states.directory, DIRECTORY_LEN);
          strncpy(states.directory, root, DIRECTORY_LEN-1);

          // get the port number of this user from config file
          states.port = getUserPort(states.username);
          dprint("port of this user is %d\n", states.port);
          if (states.port < 0) {
            states.port = 0;
            writeSock(sock, "Failed to get user specific port number from config file. \
              File transfer disabled!\nYou have logged in!");
            continue;
          }
          
          writeSock(sock, "You have logged in!");                    
          continue;

        } else {           
          // password not correct:
          states.status = 0;
          char* message = "Failed authetication. \nPlease log in by command: login <username>\n";
          writeSock(sock, message);
          bzero(states.username, USERNAME_LEN);
          continue;
        }
            
    } 

    else if (states.status < 2) {
      // Ask for authencation first, before allowing any commands below
      char* message = "Need authencation first. Please provide your username by command: login <username>\n";
      states.status = 0;
      bzero(states.username, USERNAME_LEN);
      writeSock(sock, message); 
      continue;

    } 

    /* from here, we are authenticated !! */
    else if (strncmp(token, "cd", strlen("cd")) == 0) {
      
      
      token = strtok(NULL, delim); 
      if (!token) {commandNotComplete(sock);  continue;}

      char local_dir[DIRECTORY_LEN];
      bzero(local_dir, DIRECTORY_LEN);
      strncpy(local_dir, states.directory, DIRECTORY_LEN-1);

      // parse the argument of cd into steps of directory change
      parseCD(local_dir, token, root, sock);
    
    
      //test if the local_dir is valid and allowed, if yes, update states.directory, else reject
      if (strncmp(root, local_dir, strlen(root)) != 0) {
        // not within "root" branch, disallow!
        char* message = "Directory beyong home directory. Command Disallowed!\n";
        writeSock(sock, message); continue;
      }
      if (!opendir(local_dir)) {
        char* message = "Unable to open this directory. Command Disallowed\n";
        writeSock(sock, message); continue;
      } else {
        // change to directory
        bzero(states.directory, DIRECTORY_LEN);
        strncpy(states.directory, local_dir, DIRECTORY_LEN-1);  
        char * message = "Changed Directory to ";
        writeSockNoEnd(sock, message);
        writeSock(sock, states.directory); continue;
      } 

    } 

    else if (strcmp(token, "ls") == 0) {
      
      char* message = list_directory(states.directory, 0);
      if (message == NULL) {
        writeSock(sock, "ERROR ls command did not respond!");
        continue;
      } 
      writeSock(sock, message); 
      free(message); continue;

    } 

    else if (strcmp(token, "date") == 0) {
      
      bzero(buffer, BUFFER_LEN);
      if (date_fun(buffer, BUFFER_LEN-1) < 0) {
        writeSock(sock, "ERROR date command did not respond!"); continue;
      }
      writeSock(sock, buffer); continue;

    } 

    else if (strcmp(token, "whoami") == 0) {
      
      writeSock(sock, states.username);
      //token = strtok(NULL, delim);
      //if (token != NULL) run(atoi(token));
      continue;

    } 

    else if (strcmp(token, "w") == 0) {
      
      char* message = whoIsIn(loghist);
      if (message == NULL) {
        writeSock(sock, "ERROR w command did not respond!\n");
        continue;
      }
      writeSock(sock, message);
      free (message); continue;

    } 

    else if (strcmp(token, "logout") == 0) {
            
      if (logout(loghist, states.username) < 0) {
        writeSockNoEnd(sock, "ERROR Cannot remove user from logged out list.\n \
           If you cannot re-login, contact admin :)\n");
      }

      states.status = 0; states.port = 0;
      bzero(states.username, USERNAME_LEN);
      bzero(states.directory, DIRECTORY_LEN);

      writeSock(sock, "You have logged out!\n"); 
      continue;

    } 

    else if (strcmp(token, "put") == 0) {
      
      char fname[FILENAME_LEN];
      bzero(fname, FILENAME_LEN);
      int size;
      // get file name
      token = strtok(NULL, delim); 
      if (!token) {commandNotComplete(sock);  continue;}
      strncpy(fname, token, FILENAME_LEN-1);
      // get file size
      token = strtok(NULL, delim); 
      if (!token) {commandNotComplete(sock);  continue;}
      size = atoi(token);
      if (size <= 0) {
        writeSock(sock, "File size is not positive. Command Aborted.\n"); continue;
      }
      if (states.port <= 0) {
        writeSock(sock, "ERROR cannot assign port for file transfer. Command Aborted.\n");
        continue;
      }

      // reply with the port number and call server_get() to receive and store the file
      bzero(buffer, BUFFER_LEN);
      snprintf(buffer, BUFFER_LEN-1, "put port: %d\n", states.port);
      dprint("%s\n", buffer);
      writeSockNoEnd(sock, buffer); 

      // get the port ready for reading file      
      char ffullname[DIRECTORY_LEN] = {0};
      strncpy(ffullname, states.directory, DIRECTORY_LEN-1);
      strncat(ffullname, fname, DIRECTORY_LEN -1 - strlen(ffullname) );
    
      dprint("try server_get\n");
      if (server_get(states.port, ffullname, size) != 0) {
        printf("file upload failed\n");
        writeSock(sock, "ERROR server cannot receive and store your file!\n");
      }
      continue;

    } 

    else if (strcmp(token, "get") == 0) {
      
      char fname[FILENAME_LEN]; 
      bzero(fname,FILENAME_LEN);
      token = strtok(NULL, delim);
      if (!token) {commandNotComplete(sock); continue;}
      strncpy(fname, token, FILENAME_LEN-1);
      char ffullname[DIRECTORY_LEN] = {0};
      strncpy(ffullname, states.directory, DIRECTORY_LEN-1);
      strncat(ffullname, fname, DIRECTORY_LEN -1 - strlen(ffullname) );
      
      //get the file size and test if file exists at the same time
      struct stat sb;
      if (stat(ffullname, &sb) == -1) {
        if (errno == ENOENT) { writeSock(sock, "file do not exits"); continue; } 
        else {writeSock(sock, "problem with getting file information"); continue; }
      }
      int fsize = (int)sb.st_size;
      if (fsize < 0) {writeSock(sock, "File size is somehow negative. Command aborted."); continue;}
      
      // tell the client about port and file size
      bzero(buffer, BUFFER_LEN);
      snprintf(buffer, BUFFER_LEN-1, "get port: %d size: %d\n", states.port+1, fsize);
      writeSockNoEnd(sock, buffer);

      // now send the file with server_send function and hope the client is accepting it
      if (server_send(states.port+1, ffullname, fsize) < 0 ) {
        //printf("file download failed\n");
        writeSock(sock, "ERROR problem with sending file to client!\n");
      }
      continue;

    } 

    else {

      // other command use system call
      
      char command[BUFFER_LEN] = {0};
      strncpy(command, token, BUFFER_LEN-1); 
      token = strtok(NULL, delim);
      while (token) {
        strncat(command, " ", BUFFER_LEN-1 -strlen(command));
        strncat(command, token, BUFFER_LEN-1-strlen(command) );
        token = strtok(NULL, delim);
      }
      dprint("the command is %s\n", command); fflush(stdout);
      
      /* Open the command for reading. */
      FILE *fp = popen(command, "r");
      if (fp == NULL) {
        perror("ERROR cannot popen\n");
        writeSock(sock, "ERROR cannot popen for other command\n");
        continue;
      }

      /* Read the output a line at a time - output it. */
      /* need timeout machanism here ! */
      int filedesc = fileno(fp);
      fd_set set;
      struct timeval timeout;     

      FD_ZERO(&set); /* clear the set */
      FD_SET(filedesc, &set); /* add our file descriptor to the set */

      timeout.tv_sec = 1;
      timeout.tv_usec = 0;
      do {
            int rv = select(filedesc + 1, &set, NULL, NULL, &timeout);
            if(rv == -1)
            { perror("select"); close(filedesc); pclose(fp); close(sock); exit(1); } /* an error accured */
            else if(rv == 0)
            { 
              dprint("timeout from system call\n"); 
              writeSock(sock, "Server Time Out! result maybe not complete");
              close(filedesc); break; 
            } /* a timeout occured */
            bzero(buffer, BUFFER_LEN);
            int nbyte = read(filedesc, buffer, BUFFER_LEN-1);
            if (buffer[nbyte-1] == '\0') {
                dprint("syscall terminated with a null char\n");
                writeSockNoEnd(sock, buffer);
                writeSock(sock, ""); // this adds a null char to sock
                close(filedesc); break;   
            }
            writeSockNoEnd(sock, buffer);
        
      } while(1);



      /* orignal code, without timeout 
      bzero(buffer, BUFFER_LEN);
      while (fgets(buffer, BUFFER_LEN-1, fp) != NULL) {
        dprint("reply: %s", buffer); fflush(stdout);
        writeSockNoEnd(sock, buffer);
        bzero(buffer, BUFFER_LEN);        
      }
      writeSock(sock, ""); // this adds a null char to sock
      */

      /* close */
      pclose(fp);
      dprint("returned from system call\n");
      continue;

    } /* end of if else branches */

   } /* end of while */

   close(sock);
  
} /* end of function dostuff */


int writeSockNoEnd(int sock, char* message) {
  if (write(sock, message, strlen(message)) < 0) error("ERROR writing to socket");
}

int writeSock(int sock, char* message) {
  if (write(sock, message, strlen(message)+1 ) < 0) error("ERROR writing to socket");
}

int getUserPort(char* username) {
     

     char string_0[LINE_LEN];
     char header[HEADER_LEN];
     char message1[MESSAGE1_LEN];
     char message2[MESSAGE2_LEN];
     char message3[MESSAGE3_LEN];

     FILE * file = fopen("sploit.conf","r+");
     if (file == NULL) {
        return -1;
     }

     while((fgets(string_0,LINE_LEN-1,file)) != NULL) {
      sscanf(string_0,"%s", header);
      if(strncmp(header, "user", HEADER_LEN-1) == 0) {
        sscanf(string_0, "%s %s %s %s", header, message1, message2, message3);
        if (strncmp(username, message1, USERNAME_LEN-1) == 0) { 
           fclose(file); return atoi(message3);
        }
      }
     }
     return -1;
}

void commandNotComplete(int sock) {
  char* message = "ERROR command not complete\n";
  writeSock(sock, message); 
}

void up_dir(char* initial_dir) {
  char * p = initial_dir + strlen(initial_dir) - 1;
  while (p > initial_dir) {
    *(p--) = '\0' ;
    if (*p == '/') break;
  }
  return;
}

void parseCD(char* local_dir, char* token, char* root, int sock)
{
      // take the next step in CD command
      char step[DIRECTORY_LEN] = {0};

      while ( *token != '\0' ) {
        int i = 0;
        while ( * token != '/' && *token != '\0') {
          step[i++] = *token++;
          if (i == DIRECTORY_LEN-1) break; // no overflow allowed
        }
        step[i] = '/'; step[i+1]= '\0';
        if (*token == '/') token++;

        // debug print each step
        // dprintf(sock, step);

        if (strcmp(step, "./") == 0) {
          // do nothing
        } else if (strcmp(step, "../") == 0) {
          up_dir(local_dir);
        } else if (strcmp(step, "~/") == 0) {
          bzero(local_dir, DIRECTORY_LEN);
          strcpy(local_dir, root);
        } else if (strcmp(step, "/") == 0) {
          bzero(local_dir, DIRECTORY_LEN);
          strcpy(local_dir, "/");
        } else {
          strncat(local_dir, step, DIRECTORY_LEN-1-strlen(local_dir));
        }

      }
     
}

void compound_dir(char* directory, char* loghist, char* username) {
    
    bzero(directory, DIRECTORY_LEN);
    strncpy(directory, loghist, DIRECTORY_LEN-1);
    strncat(directory, username, USERNAME_LEN-1);
    strncat(directory, "/", 1 );
}

int login(char* loghist, char* username) {
    
    char directory[DIRECTORY_LEN];
    compound_dir(directory, loghist, username);

    struct stat dir = {0};
    if (stat(directory, &dir) != -1) {
      printf("ERROR: no repeated login!\n"); return -1;
    }
    // printf("%s\n", directory);
    if (mkdir(directory, 0700)< 0) {
      printf("ERROR: error in make directory in login function\n");
      return -1;
    }
    return 0;
    
}

int logout (char* loghist, char* username) {
    char directory[DIRECTORY_LEN];
    compound_dir(directory, loghist, username);
    struct stat dir = {0};
    if (stat(directory, &dir) == -1) {
      printf("ERROR: try to logout username that is not logged in!\n");
      return -1;
    }
    if (rmdir(directory) < 0) {
      printf("ERROR: error in removing username from logged in list\n");
      return -1;
    }
    return 0;
}



char* whoIsIn(char* loghist) {
  DIR           *d;
  struct dirent *dir;

  // count howmany logins
  d = opendir(loghist);  
  int count = 0;
  if (!d) {printf("ERROR: in open directory\n"); return NULL;}
  while ((dir = readdir(d)) != NULL)
  { 
      if (strncmp(".", dir->d_name, 1) == 0) continue;
      count++;
  }
  closedir(d);
  
  if (count <= 0) {
    printf("ERROR: in getting the number of logged ins\n");
    return NULL;
  }
  
  // list all logins
  char* logins = malloc(count * USERNAME_LEN);
  bzero(logins, count * USERNAME_LEN);
  int used = 0;
  int each = 0;

  d = opendir(loghist);  
  if (!d) {printf("ERROR: in open directory\n"); return NULL;}
  while ((dir = readdir(d)) != NULL)
  { 
      if (strncmp(".", dir->d_name, 1) == 0) continue;
      snprintf(logins + used, USERNAME_LEN, "%s\n%n", dir->d_name, &each);
      used += each;
  }
  closedir(d);

  return logins;

}



bool Islogin(char* loghist, char* username) {
    char directory[DIRECTORY_LEN];
    compound_dir(directory, loghist, username);
    struct stat dir = {0};
    if (stat(directory, &dir) == -1) return false;
    return true;
}