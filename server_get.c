#include "sploit.h"


int server_get(int portno, char* fname, int fsize) {

     struct sockaddr_in serv_addr, cli_addr;
     int sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) {perror("ERROR opening socket"); return(-1);}
     
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
         { perror("ERROR on binding"); return(-1);}
     listen(sockfd,5);
     socklen_t clilen = sizeof(cli_addr);

     int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
     if (newsockfd < 0) { perror("ERROR on accept"); return(-1);}
     
     close(sockfd);


    // prepare space for reading file data
    char* buffer = malloc(fsize + 1);
    bzero(buffer, fsize + 1);

    int nget = 0;

    do {
        fd_set set;
        struct timeval timeout;
        
        FD_ZERO(&set);
        FD_SET(newsockfd, &set); 

        timeout.tv_sec = 2;
        timeout.tv_usec = 0;

        int rv = select(newsockfd + 1, &set, NULL, NULL, &timeout);
        if(rv == -1)
        { perror("select"); close(newsockfd); free(buffer); return -1; } // an error accured 
        else if(rv == 0)
        { perror("timeout"); close(newsockfd); break; }// a timeout occured 
        
        // read from sockfd
        int nbytes = read(newsockfd, buffer+nget, fsize-nget);
        if (nbytes < 0) { perror("ERROR reading from socket"); free(buffer); close(newsockfd); return(-1);}
        if (nbytes < fsize-nget) { nget += nbytes; continue;}
        else { nget += nbytes; break;}

    } while(1);
    
    if (nget == 0) return -1; // timeout with nothing got

    // read what we got to file
    // if fname is longer than FILENAME_LEN, chunk it
    if (strlen(fname) >= FILENAME_LEN) {
        fname[FILENAME_LEN-1] = '\0';
    }
    FILE *fp = fopen(fname, "wb"); 
    if(NULL == fp) {perror("ERROR creating file\n"); free(buffer); close(newsockfd); return(-1);}
    fwrite(buffer, 1, nget, fp);

    // after finish
    free(buffer);
    fclose(fp);
    close(newsockfd);
    return 0;

/*

    // this is the reading part, add timeout mechanism
	fd_set set;
  	struct timeval timeout; 	

  	FD_ZERO(&set); // clear the set 
  	FD_SET(newsockfd, &set); // add our file descriptor to the set 

  	timeout.tv_sec = 0;
  	timeout.tv_usec = 5000;

  	int rv = select(newsockfd + 1, &set, NULL, NULL, &timeout);
  	if(rv == -1)
    { perror("select"); close(newsockfd); return -1; } // an error accured 
  	else if(rv == 0)
    { perror("timeout"); close(newsockfd); return -1; }// a timeout occured 
  	
  	 // file transfer start in time, put file in a buffer (with time out)
     char* buffer = malloc(fsize + 1);
     bzero(buffer, fsize + 1);

     int nget = 0; 
     int n; 
     // set a timer for while loop     
     time_t start = time(NULL);
     time_t endwait = start + 6; // timer is 6 sec
 
     while(start < endwait) {
     	
     	n = read(newsockfd, buffer+nget, fsize-nget);
     	if (n < 0) 
     	{ perror("ERROR reading from socket"); free(buffer); close(newsockfd); return(-1);}
     	if (n < fsize-nget) { nget += n; start = time(NULL); }
     	else break;
     }
     
     // Create file where data will be stored 
     FILE *fp;
     fp = fopen(fname, "wb"); 
     if(NULL == fp) {perror("ERROR creating file\n"); free(buffer); close(newsockfd); return(-1);}
     fwrite(buffer, 1, fsize, fp);

     // after finish
     free(buffer);
     fclose(fp);
     close(newsockfd);
     return 0;
     */

}