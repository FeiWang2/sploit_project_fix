#include "sploit.h"

int readUntilNewLine(int sock, char* buffer, int BUFFER_LENx) 
{    
    
    int buf_idx = 0;
    // read until newline
    while (buf_idx < BUFFER_LENx && 1 == read(sock, &buffer[buf_idx], 1))
    {
        if ('\n' == buffer[buf_idx]) {return 0;}
        buf_idx++;
    }
    
    // if we get here, it means the command is too long or read failed (connection lost)
    if (buf_idx == BUFFER_LENx) {
      return -1;
    } else {
      perror("ERROR reading from socket, connection probably lost!\n");
      return -2;
    }
}

int readUntilNewLineTime(int sock, char* buffer, int MAX_LENx, int secs)
{

        fd_set set;
        struct timeval timeout;     

        FD_ZERO(&set); /* clear the set */
        FD_SET(sock, &set); /* add our file descriptor to the set */

        timeout.tv_sec = secs;
        timeout.tv_usec = 0;

        int buf_idx = 0;
        int rv;

        do {
            
            if (secs > 0) rv = select(sock + 1, &set, NULL, NULL, &timeout);
            else rv = select(sock + 1, &set, NULL, NULL, NULL);

            if(rv == -1) { perror("select\n"); return -2; } /* an error accured */
            if(rv == 0 ) { dprint("timeout\n"); return 1; }  /* time out */
            if (read(sock, &buffer[buf_idx], 1) != 1) {perror("read\n"); return -2;} /* an error accured */  
            if ('\n' == buffer[buf_idx]) 
            {
                dprint("taken %d chars\n", buf_idx); 
                dprint("COMMAND: %s\n", buffer);
                fflush(stdout);
                return 0;
            } /* successful read */
            buf_idx++;

        } while(buf_idx < MAX_LENx);

        return -1; /*command too long */  
}