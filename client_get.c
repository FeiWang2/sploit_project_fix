

#include "sploit.h"

int client_get(char* ip_address, int portno, char* fname, int fsize){
    
    // start TCP connection
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) { perror("ERROR opening socket\n"); close(sockfd); return(-1);}

    struct sockaddr_in serv_addr;
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    if (inet_aton(ip_address, (struct in_addr *)(&serv_addr.sin_addr.s_addr)) == 0) 
    { perror("ERROR: invalid IP\n"); close(sockfd);  return(-1); }
    
    if (connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
        { perror("ERROR connecting\n"); close(sockfd);  return(-1);}


    // prepare space for reading file data
    char* buffer = malloc(fsize + 1);
    bzero(buffer, fsize + 1);

    int nget = 0;

    do {
        fd_set set;
        struct timeval timeout;
        
        FD_ZERO(&set);
        FD_SET(sockfd, &set); 

        timeout.tv_sec = 2;
        timeout.tv_usec = 0;

        int rv = select(sockfd + 1, &set, NULL, NULL, &timeout);
        if(rv == -1)
        { perror("select"); close(sockfd); free(buffer); return -1; } // an error accured 
        else if(rv == 0)
        { perror("timeout"); close(sockfd); break; }// a timeout occured 
        
        // read from sockfd
        int nbytes = read(sockfd, buffer+nget, fsize-nget);
        if (nbytes < 0) { perror("ERROR reading from socket"); free(buffer); close(sockfd); return(-1);}
        if (nbytes < fsize-nget) { nget += nbytes; continue;}
        else { nget += nbytes; break;}

    } while(1);
    
    if (nget == 0) return -1; // timeout with nothing got

    // read what we got to file
    FILE *fp = fopen(fname, "w"); 
    if(NULL == fp) {perror("ERROR creating file1 \n"); free(buffer); close(sockfd); return(-1);}
    fwrite(buffer, 1, nget, fp);

    // after finish
    free(buffer);
    fclose(fp);
    close(sockfd);
    return 0;


/*


    // this is the reading part, add timeout mechanism
    fd_set set;
    struct timeval timeout;     

    FD_ZERO(&set);
    FD_SET(sockfd, &set); 

    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    int rv = select(sockfd + 1, &set, NULL, NULL, &timeout);
    if(rv == -1)
    { perror("select"); close(sockfd); return -1; } 
    else if(rv == 0)
    { perror("timeout"); close(sockfd); return -1; }
 
    // file get here in time, lets read!
    char* buffer = malloc(fsize + 1);
    bzero(buffer, fsize + 1);

    int nget = 0; 
    int n; 
    // set a timer for while loop     
    time_t start = time(NULL);
    time_t endwait = start + 6; // timer is 6 sec
 
    while(start < endwait) {
        
        n = read(sockfd, buffer+nget, fsize-nget);
        if (n < 0) 
        { perror("ERROR reading from socket"); free(buffer); close(sockfd); return(-1);}
        if (n < fsize-nget) { nget += n; start = time(NULL); }
        else break;
    }

    // finished reading, now writes to file (disk)
     FILE *fp;
     fp = fopen(fname, "w"); 
     if(NULL == fp) {perror("ERROR creating file1 \n"); free(buffer); close(sockfd); return(-1);}
     fwrite(buffer, 1, fsize, fp);

     // after finish
     free(buffer);
     fclose(fp);
     close(sockfd);
     return 0;
*/
}