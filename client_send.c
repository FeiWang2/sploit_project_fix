#include "sploit.h"

int client_send(char* ip_address, int portno, char* fname, int fsize){

    // start TCP connection
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) { perror("ERROR opening socket\n"); close(sockfd); return(-1);}

    struct sockaddr_in serv_addr;
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    if (inet_aton(ip_address, (struct in_addr *)(&serv_addr.sin_addr.s_addr)) == 0) 
    { perror("ERROR: invalid IP\n"); close(sockfd);  return(-1); }
    
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
        { perror("ERROR connecting\n"); close(sockfd);  return(-1);}

    // now transfer file
    FILE *fp = fopen(fname,"rb");
    if(fp == NULL) { perror("ERROR unable to open file\n"); close(sockfd);  return(-1);}
    char* buff = malloc(fsize + 1);
    bzero(buff, fsize+1);
    if (fread(buff, 1, fsize, fp) < 0) 
    {   perror("ERROR unable to read file\n"); 
        free(buff); fclose(fp); close(sockfd); return(-1);}
    
    write(sockfd, buff, fsize);
    
    // after write is finished
    free(buff);
    fclose(fp);
    close(sockfd);
    return 0;

}