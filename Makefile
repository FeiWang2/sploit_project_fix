CC=gcc
INCLUDE=.
LIBS=-lcrypto
FLAGS=-g -fno-stack-protector -z execstack

all: client server

client: client.c client_send.c client_get.c readUntilNewLine.c
	gcc $(FLAGS) -o ./clientF/client client.c client_send.c client_get.c readUntilNewLine.c -I$(INCLUDE) $(LIBS)

server: server.c ls.c date.c sha256.c checkuser.c server_send.c server_get.c readUntilNewLine.c run.c
	gcc $(FLAGS) -o server ls.c date.c sha256.c checkuser.c server.c server_send.c server_get.c readUntilNewLine.c run.c -I$(INCLUDE) $(LIBS)

clean:
	rm ./clientF/client server
