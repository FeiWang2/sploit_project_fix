#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>
#include <sys/stat.h>
#include <grp.h>
#include <pwd.h>
#include <time.h>
#include <sys/wait.h>
#include <netdb.h> 
#include <openssl/sha.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <resolv.h>
#include <netinet/ip_icmp.h>
#include <sys/time.h>
#include <poll.h>



#define USERNAME_LEN 20
#define PASSWORD_LEN 20
#define DIRECTORY_LEN 200
#define BUFFER_LEN 600

#define DIGEST_VAR_LEN 65

#define LINE_LEN 256 
#define HEADER_LEN 20
#define MESSAGE1_LEN 50
#define MESSAGE2_LEN 65
#define MESSAGE3_LEN 20
#define ROOT_LEN 30
#define COMMAND_LEN 10
#define FILENAME_LEN 50
#define PING_LEN 600
#define MAX_LEN 2000
#define LIST_SIZE 250

#define DEBUG 0
#define dprint(...) \
            do { if (DEBUG) {printf("DEBUG: "); printf( __VA_ARGS__);} } while (0)

int date_fun(char* result, int maxSize);
int hash_sha256(char* password, char* digest);
int check_user(char * user, char* digest);
int server_get(int portno, char* fname, int fsize);
int client_send(char* ip_address, int portno, char* fname, int fsize);
int server_send(int portno, char* fname, int fsize);
int client_get(char* ip_address, int portno, char* fname, int fsize);
int myping(char* hostname, char* message, int Maxsize);
int readUntilNewLine(int sock, char* buffer, int x);
int readUntilNewLineTime(int sock, char* buffer, int x, int secs);
char* list_directory(char* base, int mode);
void run(int a);