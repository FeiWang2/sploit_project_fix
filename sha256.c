
#include "sploit.h"
/* not using this example
int main()
{
    unsigned char ibuf[] = "compute sha1";
    unsigned char obuf[SHA256_DIGEST_LENGTH];

    SHA256(ibuf, strlen(ibuf), obuf);
    

    int i;
    for (i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        printf("%02x ", obuf[i]);
    }
    printf("\n");

    return 0;
}*/

// Afterwards, md will contain the binary SHA-256 message digest. 
static bool simpleSHA256(void* input, unsigned long length, unsigned char* md)
{
    SHA256_CTX context;
    if(!SHA256_Init(&context))
        return false;

    if(!SHA256_Update(&context, (unsigned char*)input, length))
        return false;

    if(!SHA256_Final(md, &context))
        return false;

    return true;
}



int hash_sha256(char* password, char* digest_var)
{
	/* code */
	// if (argc != 2) printf("%s\n", "need two arguments");
	unsigned char md[SHA256_DIGEST_LENGTH]; // 32 bytes
    
	if(!simpleSHA256(password, strlen(password), md))
	{
    	perror("ERROR in SHA256\n");
    	return(-1);
	}


	int x;
	for(x = 0; x < SHA256_DIGEST_LENGTH; x++)
        sprintf(digest_var + x*2, "%02x", md[x]);
	// putchar( '\n' );
	
	return 0;
}




