#include "sploit.h"


int server_send(int portno, char* fname, int fsize) {
	 
     
     struct sockaddr_in serv_addr, cli_addr;
     int sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) {perror("ERROR opening socket"); return(-1);}

     // binding to the port doesn't stuck if i do this
     int iSetOption = 1;
     setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&iSetOption, sizeof(iSetOption));

     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
         { perror("ERROR on binding1"); return(-1);}
     listen(sockfd,5);
     socklen_t clilen = sizeof(cli_addr);

     // continue with TCP connection
     int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
     if (newsockfd < 0) { perror("ERROR on accept"); return(-1);}
     
     close(sockfd);

    // now transfer file
    FILE *fp = fopen(fname,"rb");
    if(fp==NULL) { perror("ERROR unable to open file\n"); close(newsockfd);  return(-1);}
    char* buff = malloc(fsize + 1);
    bzero(buff, fsize+1);
    if (fread(buff, 1, fsize, fp) < 0) 
    {   perror("ERROR unable to read file\n"); 
        free(buff); fclose(fp); close(newsockfd); return(-1);}
    
    write(newsockfd, buff, fsize);
    
    // after write is finished
    free(buff);
    fclose(fp);
    close(newsockfd);
    return 0;

}