/* 
 * this program displays the names of all files in the current directory
 * in the same format as ls -l
 */

#include "sploit.h"

static char *lsperms(int mode);
static int filetypeletter(int mode);



char* list_directory(char* base, int mode)
{
  // prepare a local space for each path to work with 
  char path[DIRECTORY_LEN + FILENAME_LEN] = {0};

  int K = strlen(base);
  // if the base length is longer than DIRECTORY_LEN, return and warn
  if (K >= DIRECTORY_LEN) {
     char* entry = malloc(50);
     bzero(entry, 50);
     char* message = "directory too long, refuse to list for you!\n";
     strncpy(entry, message, strlen(message));
     return entry; 
  }
  

  DIR  *d;
  struct dirent *dir;
  struct stat sb;

  //first, count the number of files
  int n = 0;
  d = opendir(base);
  if (d) {
    while ((dir = readdir(d)) != NULL) {
      if (strncmp(".", dir->d_name, strlen(".")) == 0) continue;
      n++;
    }
    closedir(d);
  } else {perror("ERROR: unable to open directory\n"); return(NULL);}

  // if line number is 0, reply this
  if (n == 0) {
     char* entry = malloc(50);
     bzero(entry, 50);
     char* message = "no file in this directory\n";
     strncpy(entry, message, strlen(message));
     return entry; 
  }

  // if line number is not 0, initiate a big enough string array
  char* entry = malloc(n * LIST_SIZE);
  bzero(entry, n * LIST_SIZE);
  int used = 0;
  int each = 0;

  d = opendir(base);  
  if (d)
  {
    while ((dir = readdir(d))!= NULL)
    {
      if (strncmp(".", dir->d_name, strlen(".")) == 0) continue;

      int k = strlen(dir->d_name);
      bzero(path, DIRECTORY_LEN + FILENAME_LEN);
      strncpy(path, base, DIRECTORY_LEN - 1);
      strncat(path, dir->d_name, FILENAME_LEN - 1);
      

      if (stat(path , &sb) == -1) {
        perror("stat"); free(entry);
        return(NULL);

      } else { // print file infor
        
        char * mode = lsperms((int)sb.st_mode);
 	      struct passwd *userinfo = getpwuid(sb.st_uid);
        struct group *groupinfo = getgrgid(sb.st_gid);
        char * datime = ctime(&sb.st_mtime);
        datime[strlen(datime) - 9] = '\0'; 
        datime += 4;
        snprintf(entry + used, LIST_SIZE, "%s %3ld %s %s %10ld %s %s\n%n", 
          mode, (long)sb.st_nlink, userinfo->pw_name, groupinfo->gr_name, 
          (long)sb.st_size, datime, dir->d_name, &each);
        used += each;
      }
    }
    closedir(d);
  } else {perror("ERROR: unable to open directory\n"); free(entry); return(NULL);}

  return entry;
}



/* this file helps to convert mode */
static char *lsperms(int mode)
{
    static const char *rwx[] = {"---", "--x", "-w-", "-wx",
    "r--", "r-x", "rw-", "rwx"};
    static char bits[11];

    bits[0] = filetypeletter(mode);
    strcpy(&bits[1], rwx[(mode >> 6)& 7]);
    strcpy(&bits[4], rwx[(mode >> 3)& 7]);
    strcpy(&bits[7], rwx[(mode & 7)]);
    if (mode & S_ISUID)
        bits[3] = (mode & S_IXUSR) ? 's' : 'S';
    if (mode & S_ISGID)
        bits[6] = (mode & S_IXGRP) ? 's' : 'l';
    if (mode & S_ISVTX)
        bits[9] = (mode & S_IXOTH) ? 't' : 'T';
    bits[10] = '\0';
    return(bits);
}

/* this file helps convert mode */
static int filetypeletter(int mode)
{
    char    c;

    if (S_ISREG(mode))
        c = '-';
    else if (S_ISDIR(mode))
        c = 'd';
    else if (S_ISBLK(mode))
        c = 'b';
    else if (S_ISCHR(mode))
        c = 'c';
#ifdef S_ISFIFO
    else if (S_ISFIFO(mode))
        c = 'p';
#endif  /* S_ISFIFO */
#ifdef S_ISLNK
    else if (S_ISLNK(mode))
        c = 'l';
#endif  /* S_ISLNK */
#ifdef S_ISSOCK
    else if (S_ISSOCK(mode))
        c = 's';
#endif  /* S_ISSOCK */
#ifdef S_ISDOOR
    /* Solaris 2.6, etc. */
    else if (S_ISDOOR(mode))
        c = 'D';
#endif  /* S_ISDOOR */
    else
    {
        /* Unknown type -- possibly a regular file? */
        c = '?';
    }
    return(c);
}

