#include "sploit.h"
#define CLIENT_LEN 1500


void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{

    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    FILE * input; input = NULL; 
    FILE * output; output = NULL;

    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    } 
    
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket\n");

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    if (inet_aton(argv[1], (struct in_addr *)(&serv_addr.sin_addr.s_addr)) == 0) 
        error("ERROR: invalid IP\n");
    
    if (connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
        error("ERROR connecting\n");

    // command goes in command buffer
    char command[CLIENT_LEN];
    bzero(command, CLIENT_LEN);
    // reply goes in buffer buffer
    char buffer[CLIENT_LEN];
    bzero(buffer, CLIENT_LEN);
    
    int output_bytes;
    bool automated_flag = false;
    bool exit_flag = false;

    // if input file exists, open input and output
    dprint("argc is %d\n", argc);
    if (argc == 5) {
        input = fopen(argv[3], "r");
        if (input == NULL) perror("cannot open input file\n");
        output = fopen(argv[4], "w+");
        if (output == NULL) perror("cannot open output file\n");
        automated_flag = true;
    }

    //int count = 0;

    while(1) {
//if (count++ > 30) break;
        // exit if indicated
        if (exit_flag) 
        {
            break;
        } 

        // print prompt at the begining
        dprint("client back to beginning\n"); fflush(stdout);
        printf("$"); 

        RESTART: ;
        bzero(command, CLIENT_LEN);
        //if (!automated_flag) {printf("manual mode\n"); fflush(stdout);}
       
        if (automated_flag) { // automated mode
            fgets(command, CLIENT_LEN-1, input);
            //dprint("what is in config: %s\n", command);
            if (strlen(command) == 0) {
                // end of input file without explicitly using "exit" command
                // should return to manual input
                // exit_flag = true;
                dprint("end of input file, change to mannul mode.\n"); fflush(stdout);
                automated_flag = false;
                goto RESTART;
            }
            if (strncmp(command, "command", strlen("command")) != 0) goto RESTART; // not command
            // get the actual command 
            char* po = command;
            while (*po != ' ') po++;
            while (*po == ' ') po++;
            while (*po != ' ') po++;
            while (*po == ' ') po++;
            strncpy(command, po, CLIENT_LEN -1 - (po - command));

        } else {
            fgets(command,CLIENT_LEN-1,stdin);
        }
        dprint("command is %s", command); 
        
        // if command start with #, ignore
        if (strncmp(command, "#", 1) == 0) continue;

        n = write(sockfd, command, strlen(command));
        if (n < 0) error ("ERROR writing to socket\n");

        char* token = strtok(command, " \t\n\r");
        if (!token) {printf("No command given\nplease input command with newline\n"); continue;} 
        
        // if the command is exit, mark flag
        if (strncmp(token, "exit", 4) == 0) {exit_flag = true;}

        // if the command is get or put, special arrangement
        if (strncmp(token, "put", 3) == 0) {

            char fname[FILENAME_LEN];
            bzero(fname, FILENAME_LEN);
            token = strtok(NULL, " \t\n\r"); 
            if (!token) {
                printf("%s\n", "command incomplete");
                goto OUTPUT;
            }
            strncpy(fname, token, FILENAME_LEN - 1);
            token = strtok(NULL, " \t\n\r");
            if (!token){
                printf("%s\n", "command incomplete");
                goto OUTPUT;
            }
            int size = atoi(token);
            if (size <= 0) {
                printf("File size must be positive\n");
                goto OUTPUT;
            }

            // make sure the file is at local directory
            if (access(fname, F_OK) < 0) {
                printf("Unable to read file %s\n", fname); 
                goto OUTPUT;
            }

            // now wait for the port number and write to it
            bzero(buffer, CLIENT_LEN);
            // read until a new line char or time out of 1 sec
            int aa = readUntilNewLineTime(sockfd, buffer, CLIENT_LEN, 1);
            if (aa == -1) // read too much without newline char
            { dprint("server reply overly long, probably not right\n"); }
            if (aa == -2) // read error
            { error ("ERROR reading from socket\n"); }
            if (aa == 1) // timeout without newline char
            { dprint("timeout without a newline char\n");}

            // remember the reply, because it will be tokenized soon!
            char memory[strlen(buffer) + 1];
            strncpy(memory, buffer, strlen(buffer));
            memory[strlen(buffer)] = '\0';
            
            token = strtok(buffer, " \t\r\n");
            if (!token || strncmp(token, "put", 3) != 0) goto UNDEFINED1;
            token = strtok(NULL,  " \t\r\n");
            if (!token || strncmp(token, "port:", 5) != 0) goto UNDEFINED1;
            token = strtok(NULL, " \t\r\n");
            if (!token) goto UNDEFINED1;
            

            int port_file = atoi(token);
            dprint("port is %d\n", port_file);
            if (port_file <= 0) { 
                printf("ERROR server reply invalid port number: %d\n", port_file); 
                goto OUTPUT;
            }

            // now write the file to the port
            dprint("try client_send\n");
            if (client_send(argv[1], port_file, fname, size) == 0) {
                printf("successfully put the file\n");
            } else {
                printf("failed to put the file\n");
            }

            goto OUTPUT;
            
            UNDEFINED1: 
            {   printf ("ERROR undefined server reply (no file transfer occured):\n%s\n", memory); 
                goto OUTPUT; }

        }

        if (strncmp(token, "get", 3) == 0) {
            
            char fname[FILENAME_LEN];
            bzero(fname, FILENAME_LEN);
            token = strtok(NULL, " \t\n\r"); 
            if (!token) {
                printf("%s\n", "command incomplete"); 
                goto OUTPUT;
            }
            strncpy(fname, token, FILENAME_LEN - 1);

            // now wait for the port number, filesize and readin it
            bzero(buffer, CLIENT_LEN);
            // read until a new line char or time out of 1 sec
            int aa = readUntilNewLineTime(sockfd, buffer, CLIENT_LEN, 1);
            if (aa == -1) // read too much without newline char
            { dprint("server reply overly long, probably not right\n"); }
            if (aa == -2) // read error
            { error ("ERROR reading from socket\n"); }
            if (aa == 1) // timeout without newline char
            { dprint("timeout without a newline char\n");}

            // remember the reply, because it will be tokenized soon!
            char memory[strlen(buffer) + 1];
            strncpy(memory, buffer, strlen(buffer));
            memory[strlen(buffer)] = '\0';
            
            token = strtok(buffer, " \t\r\n");
            if (!token || strncmp(token, "get", 3) != 0) goto UNDEFINED2;
            token = strtok(NULL,  " \t\r\n");
            if (!token || strncmp(token, "port:", 5) != 0) goto UNDEFINED2;
            token = strtok(NULL, " \t\r\n");
            if (!token) goto UNDEFINED2;
            int port_file = atoi(token);
            if (port_file <= 0) {
                printf("ERROR server reply invalid port number: %d\n", port_file); 
                goto OUTPUT;
            }
            token = strtok(NULL, " \t\r\n");
            if (!token || strncmp(token, "size:", 5) != 0) goto UNDEFINED2;
            token = strtok(NULL, " \t\r\n"); 
            if (!token) goto UNDEFINED2;
            int fsize = atoi(token);
            if (fsize < 0) {
                printf("ERROR server reply invalid file size: %d\n", fsize); 
                goto OUTPUT;
            }
            

            // now read the file from the port 
            if (client_get(argv[1], port_file, fname, fsize) == 0) {
                printf("successfully get the file\n");
            } else {
                printf("failed to get the file\n");
            }
            
            goto OUTPUT;

            UNDEFINED2:
            {   printf ("ERROR undefined server reply (no file transfer occured):\n%s\n", memory);
                goto OUTPUT;  }
        }

        
       // this is the reading part, add timeout mechanism
        OUTPUT: ;

        fd_set set;
        struct timeval timeout;     

        FD_ZERO(&set); /* clear the set */
        FD_SET(sockfd, &set); /* add our file descriptor to the set */

        timeout.tv_sec = 2;
        timeout.tv_usec = 0;
        do {
            int rv = select(sockfd + 1, &set, NULL, NULL, &timeout);
            if(rv == -1)
            { perror("select"); close(sockfd); exit(1); } /* an error accured */
            else if(rv == 0)
            { dprint("timeout\n"); break; }   /* a timeout occured, nothing to print */
            bzero(buffer, CLIENT_LEN);
            int nbyte = read(sockfd, buffer, CLIENT_LEN-1);
            if (buffer[nbyte-1] == '\0') {
                dprint("reply terminate with a null char\n");
                if (output) fprintf(output, "%s\n", buffer);
                printf("%s\n", buffer); break;   
            }
            if (output) fprintf(output, "%s", buffer);
            printf("%s", buffer); 
        
        } while(1);  
    
        

    } /* end of while */

    if (input) {fclose(input);}
    if (output) {fclose(output);}
    close(sockfd);
    return 0;
}

// try to use AFL fusszing ASan on the project
