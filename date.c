// this function gives you the date and time

#include "sploit.h"

int date_fun(char* result, int max_size)
{	
    time_t current_time = time(NULL);
    
    if (current_time == ((time_t)-1))
    {
        perror("ERROR: Failure to obtain the current time.\n");
        return(-1);
    }

    /* Convert to local time format. */
    char* c_time_string = ctime(&current_time);

    if (c_time_string == NULL)
    {
        perror("ERROR: Failure to convert the current time.\n");
        return(-1);
    }

    struct tm lt = {0};
    localtime_r(&current_time, &lt);

	// printf("The time zone is '%s'.\n", lt.tm_zone);

	// put timezone in c_time_string
	int length = strlen(c_time_string) + strlen(lt.tm_zone) + 3 ;
	
	if (length > max_size) {
		perror("ERROR: result of date function is longer than MESSAGE_LEN-1");
		return(-1);
	}

	bzero(result, max_size);

	char * p = c_time_string + strlen(c_time_string) - 6;
	*p = '\0';
	strncpy(result, c_time_string, strlen(c_time_string));
	strncat(result, " ", 1);
	strncat(result, lt.tm_zone, strlen(lt.tm_zone));
	strncat(result, " ", 1);
	strncat(result, p+1, 5);


    /* Print to stdout. ctime() has already added a terminating newline character. */
    return 0;
    
}

