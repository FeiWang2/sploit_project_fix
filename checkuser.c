#include "sploit.h"


int check_user(char * user, char* digest){

	char header[HEADER_LEN];
  char username[USERNAME_LEN];
	char password[DIGEST_VAR_LEN];
	char string_0[LINE_LEN];

	FILE * file = fopen("sploit.conf","r+");
  if (file == NULL) {
    return -1;
  }

	while((fgets(string_0,LINE_LEN-1,file)) != NULL) {

 		 //scans the line then sets 1st argvnd 2nd word to those variables
  		sscanf(string_0,"%s %s %s", header, username, password);

  		if(strncmp(header,"user", HEADER_LEN-1) == 0 && 
  			(strncmp(username, user, USERNAME_LEN-1)==0 || 
  			 strncmp(username,"fakeuser", USERNAME_LEN-1)==0  	)) {
    		strncpy(digest, password, DIGEST_VAR_LEN - 1);
    		fclose(file);
    		return 0;
  		} 
	}

	fclose(file);
  return 0;

}
